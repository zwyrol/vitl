<?php

use App\User;
use Illuminate\Database\Seeder;

class CreateUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo 'Running Create Users Seeder. This might take a while...' . "\n";

        $handle = fopen('./storage/data/names.csv', "r");

        $usersCreated = 0;
        while ($line = fgetcsv($handle, 1000, ",")) {
            list($firstName, $lastName) = explode(" ", preg_replace('/\s+/', ' ', $line[0]));

            User::create([
                             'first_name' => $firstName,
                             'last_name'  => $lastName,
                         ]);

            $usersCreated++;
        }

        echo 'Users seeded: ' . $usersCreated . "\n";
    }
}
