#!/bin/bash

cd /

./run.sh
/etc/init.d/mysql start

cd /app
mysql -uroot -e "create database laravel"

composer install


php artisan key:generate
php artisan config:cache
php artisan migrate
php artisan db:seed --class=CreateUsers
php artisan test

/bin/bash
