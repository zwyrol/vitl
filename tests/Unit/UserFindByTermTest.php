<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;

class UserFindByTermTest extends TestCase
{
    public function testFindingUsersWithDuplicates()
    {
        $terms = [
            'Yvonne Wallace',
            'Vanessa Thomson'
        ];

        foreach ($terms as $term) {
            $this->assertEquals(
                User::findByTermsAndDupes($term, true)->count(), 2
            );
        }
    }

    public function testFindingUsersWithoutDuplicates()
    {
        $terms = [
            'Yvonne Wallace',
            'Vanessa Thomson'
        ];

        foreach ($terms as $term) {
            $this->assertEquals(
                User::findByTermsAndDupes($term, false)->count(), 1
            );
        }
    }

    public function testFindingUsers()
    {
        $terms = [
            'Hannah Bailey',
            'Jack Arnold',
            'Rose Allan'
        ];

        foreach ($terms as $term) {
            list($firstName, $lastName) = explode(' ', $term);
            $this->assertEquals(
                User::findByTermsAndDupes($term, false)->toArray()[0],
                ['first_name' => $firstName, 'last_name' => $lastName]
            );
        }
    }

    public function testDoNotExistingEntries()
    {
        $notExistingNames = [
            'I-don\'t-exists',
            'strange-name',
            1230505
        ];

        foreach ($notExistingNames as $notExistingName) {
            $this->assertEquals(
                User::findByTermsAndDupes($notExistingName, false)->toArray(), []
            );
        }
    }
}
