<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function findByTermsAndDupes(Request $request)
    {
        $terms = $request->get('terms', false);
        $dupes = $request->get('dupes', false);

        if (!$terms) {
            return [];
        }

        return User::findByTermsAndDupes($terms, $dupes);
    }
}
