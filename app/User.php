<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @param string $terms
     * @param bool   $dupes
     *
     * @return array
     */
    public static function findByTermsAndDupes(
        $terms,
        $dupes
    )
    {
        $users = User::select('first_name', 'last_name');

        if (!$dupes) {
            $users->distinct();
        }

        return $users->where("first_name", "like", "%" . $terms . "%")
                     ->orWhere("last_name", "like", "%" . $terms . "%")
                     ->orWhere(DB::raw('CONCAT_WS(" ", first_name, last_name)'), "like", "%" . $terms . "%")
                     ->orderBy('last_name', 'asc')
                     ->orderBy('first_name', 'asc')
                     ->get();
    }
}
