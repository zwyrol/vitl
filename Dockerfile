FROM mattrayner/lamp:latest-1804

COPY .env.example .env
COPY docker/run.sh run.sh
COPY docker/start.sh start
COPY docker/vhost /etc/apache2/sites-available/000-default.conf
COPY . /var/www/app

RUN chmod +x run.sh
RUN chmod +x start

